# RoleBot

A discord bot that deals with role stuff, like custom role colors and pronoun roles.

---

Due to Discord Developer ToS, I am required to tell you that RoleBot DOES log command usage and errors.

Invite link: https://discordapp.com/api/oauth2/authorize?client_id=418531152240967680&permissions=268435456&scope=bot

Just add it to your server, give it Manage Roles permission (should have that by default unless you uncheck it during invite), and it's good to go. 

Currently there's no way to enable or disable any particular feature.