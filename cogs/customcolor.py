import discord
from discord.ext import commands


class CustomColor:
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.command(aliases=['servercolors', 'servercolours', 'guildcolours'])
    @commands.cooldown(1, 30, commands.BucketType.channel)
    async def guildcolors(self, ctx):
        """Gets the current colors of the guild.

        This list can only be get every 30 secs per channel."""
        colors_in_guild = self.bot.rolebot_guild_colors(ctx)
        swooshy_guild_colors = ', '.join(colors_in_guild)
        await ctx.send(f"{len(colors_in_guild)} colors:\n"
                       f"```{swooshy_guild_colors}```")

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command(aliases=['colourme', 'color', 'colour'])
    @commands.cooldown(1, 10, commands.BucketType.user)
    async def colorme(self, ctx, color: discord.Color,
                      user: discord.Member = None):
        """Gives you a custom color role, takes hex or named colors.

        See the factory methods on
        https://discordpy.readthedocs.io/en/rewrite/api.html#colour
        for a list of named colors.

        Also, you can use the syntax "rb!color <color> <user>" if you have
        Manage Roles permission to set another user's color.

        This command can only be used every 10 seconds per user."""

        if not user:
            user = ctx.author
        user_has_perms = await self.bot.owner_or_manage_roles(ctx)
        if user != ctx.author and not user_has_perms:
            return await ctx.send(f"{ctx.author.mention}: "
                                  "To be able to set others' "
                                  "colors, you need Manage Roles permission.")
        role_name = f"rolebot-{str(color)}"
        color_role = await self.bot.get_role(ctx, role_name, True, True, color)
        if color_role in user.roles:
            await user.remove_roles(color_role)
            await self.bot.remove_role_if_no_users(color_role)
            return await ctx.send(f"{user.mention}: Removed color.")
        elif not color_role:
            return await ctx.send(f"{user.mention}: "
                                  "RoleBot reached max color limit of "
                                  f"{self.bot.rolebot_role_limit}.")

        await self.bot.remove_all_rolebot_color_roles(user)

        await user.add_roles(color_role)
        msg = f"Successfully set role color for {user.mention}."
        embed = discord.Embed(colour=color, description=msg)
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(CustomColor(bot))
