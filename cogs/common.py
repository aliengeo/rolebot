import asyncio
import discord
from discord.ext import commands

# Hey, you! Yes, you.
# I know that this isn't a complete list of pronouns
# I don't want to leave out anyone at all, so please send
# a PR to me, and I'll accept it if I consider the pronoun real.
# By real, I don't mean "ree only she/he/they", I mean "no attack helicopter".
# The list is based on http://pronoun.is/
PRONOUNS = ["she/her",
            "he/him",
            "they/them",
            "it/its",
            "any pronoun",
            "ze/hir",
            "ze/zir",
            "xey/xyr",
            "pluralist",  # This isn't a pronoun but still, some might need it.
            "sie/hir",
            "ey/em",
            "e/em",
            "hu/hum",
            "peh/pehm",
            "per/per",
            "thon/thon",
            "jee/jem",
            "ve/ver",
            "xe/xem",
            "zie/zir",
            "ze/zem",
            "zie/zem",
            "ze/mer",
            "se/sim",
            "zme/zmyr",
            "ve/vem",
            "zee/zed",
            "fae/faer",
            "zie/hir",
            "si/hyr",
            "kit/kits",
            "ne/nem",
            "ae/aer"]

# Heyo, this list is short and incomplete.
# If you want this improved, send a MR. I'll gladly merge.
SEXUALITY = ["Homosexual (Lesbian)",
             "Homosexual (Gay)",
             "Heterosexual",
             "Homoromantic (Lesbian)",
             "Homoromantic (Gay)",
             "Heteroromantic",
             "Asexual",
             "Aromantic",
             "Bisexual",
             "Pansexual",
             "Biromantic",
             "Panromantic",
             "Polyamorous",
             "Monoamorous",
             "Questioning Sexuality"]


class Common:
    def __init__(self, bot):
        self.bot = bot
        self.bot.rolebot_role_limit = 100
        self.bot.individual_pronoun_limit = 5
        self.bot.individual_sexuality_limit = 5

        self.bot.remove_role_if_no_users = self.remove_role_if_no_users
        self.bot.rolebot_check_limit = self.rolebot_check_limit
        self.bot.rolebot_guild_colors = self.rolebot_guild_colors
        self.bot.get_role = self.get_role
        self.bot.remove_all_rolebot_color_roles = \
            self.remove_all_rolebot_color_roles
        self.bot.user_pronouns = self.user_pronouns
        self.bot.user_sexualities = self.user_sexualities
        self.bot.PRONOUNS = PRONOUNS
        self.bot.SEXUALITY = SEXUALITY
        self.bot.async_call_shell = self.async_call_shell
        self.bot.slice_message = self.slice_message
        self.bot.clear_roles = self.clear_roles
        self.max_split_length = 3
        self.bot.remove_all_user_pronoun_roles = \
            self.remove_all_user_pronoun_roles
        self.bot.remove_all_user_sexuality_roles = \
            self.remove_all_user_sexuality_roles
        self.bot.owner_or_manage_roles = self.owner_or_manage_roles
        self.bot.move_roles = self.move_roles
        self.bot.get_rolebot_role = self.get_rolebot_role

    # 2000 is maximum limit of discord
    async def slice_message(self, text, size=2000, prefix="", suffix=""):
        """Slices a message into multiple messages"""
        if len(text) > size * self.max_split_length:
            haste_url = await self.haste(text)
            return [f"Message is too long ({len(text)} > "
                    f"{size * self.max_split_length} "
                    f"({size} * {self.max_split_length}))"
                    f", go to haste: <{haste_url}>"]
        reply_list = []
        size_wo_fix = size - len(prefix) - len(suffix)
        while len(text) > size_wo_fix:
            reply_list.append(f"{prefix}{text[:size_wo_fix]}{suffix}")
            text = text[size_wo_fix:]
        reply_list.append(f"{prefix}{text}{suffix}")
        return reply_list

    async def haste(self, text, instance='https://hastebin.com/'):
        response = await self.bot.aiosession.post(f"{instance}documents",
                                                  data=text)
        if response.status == 200:
            result_json = await response.json()
            return f"{instance}{result_json['key']}"

    async def async_call_shell(self, shell_command: str,
                               inc_stdout=True, inc_stderr=True):
        pipe = asyncio.subprocess.PIPE
        proc = await asyncio.create_subprocess_shell(str(shell_command),
                                                     stdout=pipe,
                                                     stderr=pipe)

        if not (inc_stdout or inc_stderr):
            return "??? you set both stdout and stderr to False????"

        proc_result = await proc.communicate()
        stdout_str = proc_result[0].decode('utf-8').strip()
        stderr_str = proc_result[1].decode('utf-8').strip()

        if inc_stdout and not inc_stderr:
            return stdout_str
        elif inc_stderr and not inc_stdout:
            return stderr_str

        if stdout_str and stderr_str:
            return f"stdout:\n\n{stdout_str}\n\n"\
                   f"======\n\nstderr:\n\n{stderr_str}"
        elif stdout_str:
            return f"stdout:\n\n{stdout_str}"
        elif stderr_str:
            return f"stderr:\n\n{stderr_str}"

        return "No output."

    # Yeah sure repeated code but I give no fucks at this point
    # I just want this to work ugh
    # Why are checks this much of a fuck
    async def owner_or_manage_roles(self, ctx):
        return (ctx.author == ctx.bot.app_info.owner or
                ctx.author.guild_permissions.manage_roles)

    async def owner_or_manage_roles_check(ctx):
        return (ctx.author == ctx.bot.app_info.owner or
                ctx.author.guild_permissions.manage_roles)

    async def move_roles(self, ctx):
        """Moves roles right under its own role, this is the backend."""
        for role in ctx.guild.roles:
            if role.name in PRONOUNS or role.name.startswith("rolebot-"):
                our_role = await self.get_rolebot_role(ctx)
                our_pos = our_role.position
                new_pos = our_pos - 1
                new_pos = 1 if new_pos <= 0 else new_pos
                await role.edit(position=new_pos)
        return True

    async def clear_roles(self, ctx, dryrun=False):
        """Clears roles, this is the backend."""
        roles_deleted = 0
        for role in ctx.guild.roles:
            if role.name in PRONOUNS or role.name.startswith("rolebot-"):
                if dryrun:
                    roles_deleted += 0 if role.members else 1
                else:
                    remove_result = await self.bot.remove_role_if_no_users(role)
                    roles_deleted += 1 if remove_result else 0
        return roles_deleted

    @commands.bot_has_permissions(manage_roles=True)
    @commands.check(owner_or_manage_roles_check)
    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.guild)
    async def clearroles(self, ctx, dryrun=False):
        """Removes unused rolebot roles in guild. (needs Manage Roles)

        This command can only be used every 60 secs per guild."""
        roleclear = await self.clear_roles(ctx, dryrun)
        if roleclear is None:
            return await ctx.send("I had some sort of issue when clearing "
                                  "roles. It was logged, devs will check "
                                  "it out. Sorry.")

        dryrun_text = "would be deleted outside dryrun" if dryrun else "deleted"

        await ctx.send(f"{ctx.message.author.mention}: Done, {roleclear} "
                       f"unused rolebot role(s) {dryrun_text}.")

    @commands.bot_has_permissions(manage_roles=True)
    @commands.check(owner_or_manage_roles_check)
    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.guild)
    async def moveroles(self, ctx):
        """Moves rolebot roles under rolebot role. EXPERIMENTAL (needs Manage Roles)

        This command can only be used every 60 secs per guild."""
        if await self.move_roles(ctx):
            await ctx.send(f"{ctx.message.author.mention}: Successfully "
                           "moved roles.")

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command(name="import")
    @commands.cooldown(1, 60, commands.BucketType.user)
    async def _import(self, ctx, guild_id: int):
        """Imports rolebot roles from another guild.

        This command can only be used every 60 secs per user."""
        mentioned_guild = self.bot.get_guild(guild_id)
        guild_member = None
        if mentioned_guild in self.bot.guilds:
            maybe_member = mentioned_guild.get_member(ctx.author.id)
            if maybe_member:
                guild_member = maybe_member
            else:
                return await ctx.send(f"{ctx.message.author.mention}:"
                                      "You're not in that guild")
        else:
            return await ctx.send(f"{ctx.message.author.mention}: "
                                  "I'm not in that guild")
        self.bot.log.info(guild_member)

        pronoun_names = [pronoun.name for pronoun in
                         self.user_pronouns(guild_member)]

        sexuality_names = [sexuality.name for sexuality in
                           self.user_sexualities(guild_member)]
        rroles = self.user_rolebot_roles(guild_member)

        # Remove all rolebot roles and pronouns the user has
        await self.bot.remove_all_rolebot_color_roles(ctx.author)
        await self.bot.remove_all_user_pronoun_roles(ctx.author)
        await self.bot.remove_all_user_sexuality_roles(ctx.author)

        # Do checks
        if len(pronoun_names) >= self.bot.individual_pronoun_limit:
            return await ctx.send(f"{ctx.message.author.mention}: You have "
                                  "more pronouns than the limit, I can't "
                                  "import this.")
        if len(sexuality_names) >= self.bot.individual_sexuality_limit:
            return await ctx.send(f"{ctx.message.author.mention}: You have "
                                  "more pronouns than the limit, I can't "
                                  "import this.")

        # Add roles from other guilds.
        for pronoun in pronoun_names:
            pronoun_role = await self.get_role(ctx, pronoun, False)
            await ctx.author.add_roles(pronoun_role)
        for sexuality in sexuality_names:
            sexuality_role = await self.get_role(ctx, sexuality, False)
            await ctx.author.add_roles(sexuality_role)

        for rrole in rroles:
            role = await self.get_role(ctx, rrole.name, True, True, rrole.color)
            if not role:
                return await ctx.send(f"{ctx.message.author.mention}: Guild "
                                      "reached max role limit, can't import.")
            await ctx.author.add_roles(role)

        added_count = len(pronoun_names) + (len(sexuality_names) + len(rroles)

        await ctx.send(f"{ctx.message.author.mention}: Done, "
                       f"{added_count} role(s) added.")

    def user_pronouns(self, user: discord.Member):
        """Returns the list of pronouns of a Member."""
        user_pronouns = []
        for role in user.roles:
            if role.name in PRONOUNS:
                user_pronouns.append(role)
        return user_pronouns

    def user_sexualities(self, user: discord.Member):
        """Returns the list of sexuality roles of a Member."""
        user_sexualities = []
        for role in user.roles:
            if role.name in SEXUALITY:
                user_sexualities.append(role)
        return user_sexualities

    def user_rolebot_roles(self, user: discord.Member):
        """Returns the list of rolebot roles of a Member. (except pronouns)"""
        user_rroles = []
        for role in user.roles:
            if role.name.startswith("rolebot-"):
                user_rroles.append(role)
        return user_rroles

    async def remove_role_if_no_users(self, role: discord.Role):
        if not role.members:
            await role.delete()
            return True
        return False

    def rolebot_check_limit(self, ctx) -> bool:
        """Checks if rolebot limit is used up in the guild"""
        rolebot_roles = 0
        for role in ctx.guild.roles:
            if role.name.startswith("rolebot-"):
                rolebot_roles += 1
        return rolebot_roles >= self.bot.rolebot_role_limit

    def rolebot_guild_colors(self, ctx):
        """Returns the list of colors in the guild."""
        guild_colors = []
        for role in ctx.guild.roles:
            if role.name.startswith("rolebot-#"):
                guild_colors.append(role.name.split('-')[-1])
        return guild_colors

    # fuck my life coding at 4am
    async def get_rolebot_role(self, ctx):
        rolebot_role = discord.utils.get(ctx.guild.roles,
                                         name="RoleBot")
        if rolebot_role and ctx.me in rolebot_role.members:
            return rolebot_role
        elif rolebot_role:
            await ctx.me.add_roles(rolebot_role)
        else:
            rolebot_role = await ctx.guild.create_role(name="RoleBot")
        await ctx.me.add_roles(rolebot_role)
        return rolebot_role

    async def get_role(self, ctx, role_name: str, check_limit: bool = True,
                       create_role: bool = True, role_color=None):
        """Gets a role with name, if it can't find it, creates the role"""
        role_with_name = discord.utils.get(ctx.guild.roles, name=role_name)
        if role_with_name:
            return role_with_name

        # if we're at this point then the role doesn't exist
        if check_limit and self.rolebot_check_limit(ctx):
            return None
        if create_role:
            our_role = await self.get_rolebot_role(ctx)
            our_pos = our_role.position
            # Can we improve this?
            new_role = None
            if role_color:
                new_role = await ctx.guild.create_role(name=role_name,
                                                       colour=role_color)
            else:
                new_role = await ctx.guild.create_role(name=role_name)
            await new_role.edit(position=our_pos)
            return new_role
        return None

    async def remove_all_rolebot_color_roles(self, user: discord.User,
                                             delete_unused: bool = True):
        """Removes all rolebot color roles from a user"""
        # Make this less hacky
        for role in user.roles:
            if role.name.startswith("rolebot-#"):
                await user.remove_roles(role)
                if delete_unused:
                    await self.remove_role_if_no_users(role)

    async def remove_all_user_pronoun_roles(self, user: discord.User,
                                            delete_unused: bool = True):
        """Removes all rolebot color roles from a user"""
        # Make this less hacky
        for pronoun in self.bot.PRONOUNS:
            role_with_name = discord.utils.get(user.roles, name=pronoun)
            if role_with_name:
                await user.remove_roles(role_with_name)
                if delete_unused:
                    await self.bot.remove_role_if_no_users(role_with_name)

    async def remove_all_user_sexuality_roles(self, user: discord.User,
                                            delete_unused: bool = True):
        """Removes all rolebot sexuality roles from a user"""
        # Make this less hacky
        for sexuality in self.bot.SEXUALITY:
            role_with_name = discord.utils.get(user.roles, name=sexuality)
            if role_with_name:
                await user.remove_roles(role_with_name)
                if delete_unused:
                    await self.bot.remove_role_if_no_users(role_with_name)


def setup(bot):
    bot.add_cog(Common(bot))
