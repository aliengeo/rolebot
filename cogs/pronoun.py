from discord.ext import commands


class Pronoun:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.cooldown(1, 30, commands.BucketType.channel)
    async def pronounlist(self, ctx):
        """Gets the list of self-assignable pronouns.

        This list can only be get every 30 secs per channel."""
        swooshy_pronouns = ", ".join(self.bot.PRONOUNS)
        await ctx.send("List of self-assignable pronouns:\n"
                       f"```{swooshy_pronouns}```\n"
                       "Are your pronouns not listed here?"
                       "Please open an issue or send a PR on"
                       " <https://gitlab.com/aoz/rolebot>.")

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.user)
    async def clearpronouns(self, ctx):
        """Clears your pronoun roles.

        This list can only be used every 60 secs per user."""
        await self.bot.remove_all_user_pronoun_roles(ctx.author)
        return await ctx.send(f"{ctx.author.mention}: Done")

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command(aliases=['pronoun'])
    @commands.cooldown(1, 10, commands.BucketType.user)
    async def pronounme(self, ctx, *, pronoun: str):
        """Adds/removes a pronoun role (see rb!pronounlist).

        This command can only be used every 10 seconds per user."""
        pronoun = pronoun.strip('" \'')
        pronoun_count = len(self.bot.user_pronouns(ctx.author))
        if pronoun not in self.bot.PRONOUNS:
            return await ctx.send(f"{str(ctx.author).replace('@','')}:"
                                  " Sorry! This pronoun"
                                  " is not whitelisted. I have no intentions"
                                  " to block anyone off getting their pronouns"
                                  " though, so please send an issue or a PR on "
                                  "<https://gitlab.com/aoz/rolebot>.")
        elif pronoun_count >= self.bot.individual_pronoun_limit:
            return await ctx.send(f"{str(ctx.author).replace('@','')}: "
                                  "Sorry but I can only"
                                  f" assign {self.bot.individual_pronoun_limit}"
                                  " pronouns per user at the moment, to prevent"
                                  " abuse. I know that it's not super inclusive"
                                  ", but abuse is a big risk. \n\nTo remove"
                                  " your pronoun(s), you can do "
                                  "`rb!clearpronouns` or "
                                  "`rb!pronounme <pronoun-to-remove>`.")

        pronoun_role = await self.bot.get_role(ctx, pronoun, False)
        if pronoun_role in ctx.author.roles:
            await ctx.author.remove_roles(pronoun_role)
            await self.bot.remove_role_if_no_users(pronoun_role)
            return await ctx.send(f"{str(ctx.message.author).replace('@','')}:"
                                  " Removed pronoun.")

        await ctx.author.add_roles(pronoun_role)
        await ctx.send(f"{str(ctx.message.author).replace('@','')}: Pronoun set!")


def setup(bot):
    bot.add_cog(Pronoun(bot))
