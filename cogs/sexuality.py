from discord.ext import commands


class Sexuality:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.cooldown(1, 30, commands.BucketType.channel)
    async def sexualitylist(self, ctx):
        """Gets the list of self-assignable sexualities.

        This list can only be get every 30 secs per channel."""
        swooshy_sexualities = ", ".join(self.bot.SEXUALITY)
        await ctx.send("List of self-assignable sexualities:\n"
                       f"```{swooshy_sexualities}```\n"
                       "Is your seuxality not listed here?"
                       "Please open an issue or send a PR on"
                       " <https://gitlab.com/aoz/rolebot>.")

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.user)
    async def clearsexuality(self, ctx):
        """Clears your sexuality roles.

        This list can only be used every 60 secs per user."""
        await self.bot.remove_all_user_sexuality_roles(ctx.author)
        return await ctx.send(f"{ctx.author.mention}: Done")

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command(aliases=['sexme'])
    @commands.cooldown(1, 10, commands.BucketType.user)
    async def sexuality(self, ctx, *, sexuality: str):
        """Adds/removes a sexuality role (see rb!sexualitylist).

        This command can only be used every 10 seconds per user."""
        sexuality = sexuality.strip('" \'')
        sexuality_count = len(self.bot.user_sexualities(ctx.author))
        if sexuality not in self.bot.SEXUALITY:
            return await ctx.send(f"{str(ctx.author).replace('@','')}:"
                                  " Sorry! This sexuality"
                                  " is not whitelisted. I have no intentions"
                                  " to block anyone off getting their sexuality role"
                                  " though, so please send an issue or a PR on "
                                  "<https://gitlab.com/aoz/rolebot>.")
        elif sexuality_count >= self.bot.individual_sexuality_limit:
            return await ctx.send(f"{str(ctx.author).replace('@','')}: "
                                  "Sorry but I can only"
                                  f" assign {self.bot.individual_pronoun_limit}"
                                  " pronouns per user at the moment, to prevent"
                                  " abuse. I know that it's not super inclusive"
                                  ", but abuse is a big risk. \n\nTo remove"
                                  " your pronoun(s), you can do "
                                  "`rb!clearpronouns` or "
                                  "`rb!pronounme <pronoun-to-remove>`.")

        sexuality_role = await self.bot.get_role(ctx, sexuality, False)
        if sexuality_role in ctx.author.roles:
            await ctx.author.remove_roles(sexuality_role)
            await self.bot.remove_role_if_no_users(sexuality_role)
            return await ctx.send(f"{str(ctx.message.author).replace('@','')}:"
                                  " Removed sexuality.")

        await ctx.author.add_roles(sexuality_role)
        await ctx.send(f"{str(ctx.message.author).replace('@','')}: Sexuality set!")


def setup(bot):
    bot.add_cog(Sexuality(bot))
