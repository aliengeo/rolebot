import sys
import logging
import logging.handlers
import traceback
import configparser
from pathlib import Path
import aiohttp

import discord
from discord.ext import commands


log_file_name = "rolebot.log"

# Limit of discord (non-nitro) is 8MB (not MiB)
max_file_size = 1000 * 1000 * 8
backup_count = 10000  # random big number
file_handler = logging.handlers.RotatingFileHandler(
    filename=log_file_name, maxBytes=max_file_size, backupCount=backup_count)
stdout_handler = logging.StreamHandler(sys.stdout)

log_format = logging.Formatter(
    '[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s')
file_handler.setFormatter(log_format)
stdout_handler.setFormatter(log_format)

log = logging.getLogger('discord')
log.setLevel(logging.INFO)
log.addHandler(file_handler)
log.addHandler(stdout_handler)

config = configparser.ConfigParser()
config.read("rolebot.ini")


def get_prefix(bot, message):
    prefixes = [config['base']['prefix']]

    return commands.when_mentioned_or(*prefixes)(bot, message)


initial_extensions = ['cogs.common',
                      'cogs.admin',
                      'cogs.basic',
                      'cogs.customcolor',
                      'cogs.pronoun']

bot = commands.Bot(command_prefix=get_prefix,
                   description=config['base']['description'], pm_help=None)

bot.log = log
bot.config = config

if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            log.error(f'Failed to load extension {extension}.', file=sys.stderr)
            log.error(traceback.print_exc())


@bot.event
async def on_ready():
    aioh = {"User-Agent": "RoleBot/1.0 (https://gitlab.com/aoz/rolebot)'"}
    bot.aiosession = aiohttp.ClientSession(headers=aioh)
    bot.app_info = await bot.application_info()

    log.info(f'\nLogged in as: {bot.user.name} - '
             f'{bot.user.id}\ndpy version: {discord.__version__}\n')

    await bot.change_presence(activity=discord.Game(name=f'rb!help'))


@bot.event
async def on_command(ctx):
    log_text = f"{ctx.message.author} ({ctx.message.author.id}): "\
               f"\"{ctx.message.content}\" "
    if ctx.guild:  # was too long for tertiary if
        log_text += f"on \"{ctx.channel.name}\" ({ctx.channel.id}) "\
                    f"at \"{ctx.guild.name}\" ({ctx.guild.id})"
    else:
        log_text += f"on DMs ({ctx.channel.id})"
    log.info(log_text)


@bot.event
async def on_error(event_method, *args, **kwargs):
    log.error(f"Error on {event_method}: {sys.exc_info()}")


@bot.event
async def on_command_error(ctx, error):
    log.error(f"Error with \"{ctx.message.content}\" from "
              f"\"{ctx.message.author}\ ({ctx.message.author.id}) "
              f"of type {type(error)}: {error}")

    if isinstance(error, commands.NoPrivateMessage):
        return await ctx.send("This command doesn't work on DMs.")
    elif isinstance(error, commands.MissingPermissions):
        roles_needed = '\n- '.join(error.missing_perms)
        return await ctx.send(f"{ctx.author.mention}: You don't have the right"
                              " permissions to run this command. You need: "
                              f"```- {roles_needed}```")
    elif isinstance(error, commands.BotMissingPermissions):
        roles_needed = '\n-'.join(error.missing_perms)
        return await ctx.send(f"{ctx.author.mention}: Bot doesn't have "
                              "the right permissions to run this command. "
                              "Please add the following roles: "
                              f"```- {roles_needed}```")
    elif isinstance(error, commands.CommandOnCooldown):
        return await ctx.send(f"{ctx.author.mention}: You're being "
                              "ratelimited. Try in "
                              f"{error.retry_after:.1f} seconds.")
    elif isinstance(error, commands.CheckFailure):
        return await ctx.send(f"{ctx.author.mention}: Check failed. "
                              "You might not have the right permissions "
                              "to run this command.")

    help_text = f"Usage of this command is: ```{ctx.prefix}"\
                f"{ctx.command.signature}```\nPlease see `{ctx.prefix}help "\
                f"{ctx.command.name}` for more info about this command."
    if isinstance(error, commands.BadArgument):
        return await ctx.send(f"{ctx.author.mention}: You gave incorrect "
                              f"arguments. {help_text}")
    elif isinstance(error, commands.MissingRequiredArgument):
        return await ctx.send(f"{ctx.author.mention}: You gave incomplete "
                              f"arguments. {help_text}")


@bot.event
async def on_guild_join(guild):
    bot.log.info(f"Joined guild \"{guild.name}\" ({guild.id}).")
    await guild.owner.send("Hello and welcome to RoleBot!\n"
                           "If you don't know why you're getting this message"
                           ", it's because someone added RoleBot to your "
                           "server\nDue to Discord API ToS, I am required to "
                           "inform you that **I log command usages and "
                           "errors**.\n**I don't log *anything* else**."
                           "\n\nIf you do not agree to be logged, stop"
                           " using RoleBot and remove it from your "
                           "server as soon as possible.")


@bot.event
async def on_message(message):
    if message.author.bot:
        return

    # TODO: don't have this hardcoded
    beta_accs = [137584770145058817,
                 325080604657975298,
                 406953607934509056]

    if config['base']['prefix'] == "rbb!":
        if message.author.id not in beta_accs:
            return

    ctx = await bot.get_context(message)
    await bot.invoke(ctx)

if not Path("rolebot.ini").is_file():
    log.warning(
        "No config file (rolebot.ini) found, "
        "please create one from rolebot.ini.example file.")
    exit(3)

bot.run(config['base']['token'], bot=True, reconnect=True)
